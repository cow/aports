Patch-Source: https://github.com/php/php-src/pull/11162

From 1b562ba032181a7967b1e2e118db893adcdbeaf1 Mon Sep 17 00:00:00 2001
From: Niels Dossche <7771979+nielsdos@users.noreply.github.com>
Date: Sat, 29 Apr 2023 21:07:50 +0200
Subject: [PATCH] Fix GH-11160: Few tests failed building with new libxml
 2.11.0

It's possible to categorise the failures into 2 categories:
  - Changed error message. In this case we either duplicate the test and
    modify the error message. Or if the change in error message is
    small, we use the EXPECTF matchers to make the test compatible with both
    old and new versions of libxml2.
  - Missing warnings. This is caused by a change in libxml2 where the
    parser started using SAX APIs internally [1]. In this case the
    error_type passed to php_libxml_internal_error_handler() changed from
    PHP_LIBXML_ERROR to PHP_LIBXML_CTX_WARNING because it internally
    started to use the SAX handlers instead of the generic handlers.
    However, for the SAX handlers the current input stack is empty, so
    nothing is actually printed. I fixed this by falling back to a
    regular warning without a filename & line number reference, which
    mimicks the old behaviour. Furthermore, this change now also shows
    an additional warning in a test which was previously hidden.

[1] https://gitlab.gnome.org/GNOME/libxml2/-/commit/9a82b94a94bd310db426edd453b0f38c6c8f69f5
---
 .../DOMDocument_loadXML_error2_gte2_11.phpt   | 34 +++++++
 ...> DOMDocument_loadXML_error2_pre2_11.phpt} |  4 +
 .../DOMDocument_load_error2_gte2_11.phpt      | 34 +++++++
 ...t => DOMDocument_load_error2_pre2_11.phpt} |  4 +
 ext/libxml/libxml.c                           |  2 +
 ext/libxml/tests/bug61367-read_2.phpt         |  2 +-
 .../tests/libxml_disable_entity_loader_2.phpt |  2 +-
 ...set_external_entity_loader_variation2.phpt |  2 +
 ext/xml/tests/bug26614_libxml_gte2_11.phpt    | 95 +++++++++++++++++++
 ...bxml.phpt => bug26614_libxml_pre2_11.phpt} |  1 +
 10 files changed, 178 insertions(+), 2 deletions(-)
 create mode 100644 ext/dom/tests/DOMDocument_loadXML_error2_gte2_11.phpt
 rename ext/dom/tests/{DOMDocument_loadXML_error2.phpt => DOMDocument_loadXML_error2_pre2_11.phpt} (90%)
 create mode 100644 ext/dom/tests/DOMDocument_load_error2_gte2_11.phpt
 rename ext/dom/tests/{DOMDocument_load_error2.phpt => DOMDocument_load_error2_pre2_11.phpt} (90%)
 create mode 100644 ext/xml/tests/bug26614_libxml_gte2_11.phpt
 rename ext/xml/tests/{bug26614_libxml.phpt => bug26614_libxml_pre2_11.phpt} (96%)

diff --git a/ext/dom/tests/DOMDocument_loadXML_error2_gte2_11.phpt b/ext/dom/tests/DOMDocument_loadXML_error2_gte2_11.phpt
new file mode 100644
index 000000000000..ff5ceb3fbed5
--- /dev/null
+++ b/ext/dom/tests/DOMDocument_loadXML_error2_gte2_11.phpt
@@ -0,0 +1,34 @@
+--TEST--
+Test DOMDocument::loadXML() detects not-well formed XML
+--SKIPIF--
+<?php
+if (LIBXML_VERSION < 21100) die('skip libxml2 test variant for version >= 2.11');
+?>
+--DESCRIPTION--
+This test verifies the method detects attributes values not closed between " or '
+Environment variables used in the test:
+- XML_FILE: the xml file to load
+- LOAD_OPTIONS: the second parameter to pass to the method
+- EXPECTED_RESULT: the expected result
+--CREDITS--
+Antonio Diaz Ruiz <dejalatele@gmail.com>
+--INI--
+assert.bail=true
+--EXTENSIONS--
+dom
+--ENV--
+XML_FILE=/not_well_formed2.xml
+LOAD_OPTIONS=0
+EXPECTED_RESULT=0
+--FILE_EXTERNAL--
+domdocumentloadxml_test_method.inc
+--EXPECTF--
+Warning: DOMDocument::loadXML(): AttValue: " or ' expected in Entity, line: 4 in %s on line %d
+
+Warning: DOMDocument::loadXML(): internal error: xmlParseStartTag: problem parsing attributes in Entity, line: 4 in %s on line %d
+
+Warning: DOMDocument::loadXML(): Couldn't find end of Start Tag book line 4 in Entity, line: 4 in %s on line %d
+
+Warning: DOMDocument::loadXML(): Opening and ending tag mismatch: books line 3 and book in Entity, line: 7 in %s on line %d
+
+Warning: DOMDocument::loadXML(): Extra content at the end of the document in Entity, line: 8 in %s on line %d
diff --git a/ext/dom/tests/DOMDocument_loadXML_error2.phpt b/ext/dom/tests/DOMDocument_loadXML_error2_pre2_11.phpt
similarity index 90%
rename from ext/dom/tests/DOMDocument_loadXML_error2.phpt
rename to ext/dom/tests/DOMDocument_loadXML_error2_pre2_11.phpt
index a24d5215da48..c826386f7a4e 100644
--- a/ext/dom/tests/DOMDocument_loadXML_error2.phpt
+++ b/ext/dom/tests/DOMDocument_loadXML_error2_pre2_11.phpt
@@ -1,5 +1,9 @@
 --TEST--
 Test DOMDocument::loadXML() detects not-well formed XML
+--SKIPIF--
+<?php
+if (LIBXML_VERSION >= 21100) die('skip libxml2 test variant for version < 2.11');
+?>
 --DESCRIPTION--
 This test verifies the method detects attributes values not closed between " or '
 Environment variables used in the test:
diff --git a/ext/dom/tests/DOMDocument_load_error2_gte2_11.phpt b/ext/dom/tests/DOMDocument_load_error2_gte2_11.phpt
new file mode 100644
index 000000000000..32b6bf161142
--- /dev/null
+++ b/ext/dom/tests/DOMDocument_load_error2_gte2_11.phpt
@@ -0,0 +1,34 @@
+--TEST--
+Test DOMDocument::load() detects not-well formed 
+--SKIPIF--
+<?php
+if (LIBXML_VERSION < 21100) die('skip libxml2 test variant for version >= 2.11');
+?>
+--DESCRIPTION--
+This test verifies the method detects attributes values not closed between " or '
+Environment variables used in the test:
+- XML_FILE: the xml file to load
+- LOAD_OPTIONS: the second parameter to pass to the method
+- EXPECTED_RESULT: the expected result
+--CREDITS--
+Antonio Diaz Ruiz <dejalatele@gmail.com>
+--INI--
+assert.bail=true
+--EXTENSIONS--
+dom
+--ENV--
+XML_FILE=/not_well_formed2.xml
+LOAD_OPTIONS=0
+EXPECTED_RESULT=0
+--FILE_EXTERNAL--
+domdocumentload_test_method.inc
+--EXPECTF--
+Warning: DOMDocument::load(): AttValue: " or ' expected in %s on line %d
+
+Warning: DOMDocument::load(): internal error: xmlParseStartTag: problem parsing attributes in %s on line %d
+
+Warning: DOMDocument::load(): Couldn't find end of Start Tag book line 4 in %s on line %d
+
+Warning: DOMDocument::load(): Opening and ending tag mismatch: books line 3 and book in %s on line %d
+
+Warning: DOMDocument::load(): Extra content at the end of the document in %s on line %d
diff --git a/ext/dom/tests/DOMDocument_load_error2.phpt b/ext/dom/tests/DOMDocument_load_error2_pre2_11.phpt
similarity index 90%
rename from ext/dom/tests/DOMDocument_load_error2.phpt
rename to ext/dom/tests/DOMDocument_load_error2_pre2_11.phpt
index cd13b3f901b2..695740be9ca9 100644
--- a/ext/dom/tests/DOMDocument_load_error2.phpt
+++ b/ext/dom/tests/DOMDocument_load_error2_pre2_11.phpt
@@ -1,5 +1,9 @@
 --TEST--
 Test DOMDocument::load() detects not-well formed XML
+--SKIPIF--
+<?php
+if (LIBXML_VERSION >= 21100) die('skip libxml2 test variant for version < 2.11');
+?>
 --DESCRIPTION--
 This test verifies the method detects attributes values not closed between " or '
 Environment variables used in the test:
diff --git a/ext/libxml/libxml.c b/ext/libxml/libxml.c
index b4099bb7b822..3959b362a0e4 100644
--- a/ext/libxml/libxml.c
+++ b/ext/libxml/libxml.c
@@ -527,6 +527,8 @@ static void php_libxml_ctx_error_level(int level, void *ctx, const char *msg)
 		} else {
 			php_error_docref(NULL, level, "%s in Entity, line: %d", msg, parser->input->line);
 		}
+	} else {
+		php_error_docref(NULL, E_WARNING, "%s", msg);
 	}
 }
 
diff --git a/ext/libxml/tests/bug61367-read_2.phpt b/ext/libxml/tests/bug61367-read_2.phpt
index 8adad1ce429f..38f12949bcbb 100644
--- a/ext/libxml/tests/bug61367-read_2.phpt
+++ b/ext/libxml/tests/bug61367-read_2.phpt
@@ -56,6 +56,6 @@ bool(true)
 int(4)
 bool(true)
 
-Warning: DOMDocument::loadXML(): I/O warning : failed to load external entity "file:///%s/test_bug_61367-read/bad" in %s on line %d
+Warning: DOMDocument::loadXML(): %Sfailed to load external entity "file:///%s/test_bug_61367-read/bad" in %s on line %d
 
 Warning: Attempt to read property "nodeValue" on null in %s on line %d
diff --git a/ext/libxml/tests/libxml_disable_entity_loader_2.phpt b/ext/libxml/tests/libxml_disable_entity_loader_2.phpt
index ad253171625f..182fe13cfda9 100644
--- a/ext/libxml/tests/libxml_disable_entity_loader_2.phpt
+++ b/ext/libxml/tests/libxml_disable_entity_loader_2.phpt
@@ -39,6 +39,6 @@ bool(true)
 Deprecated: Function libxml_disable_entity_loader() is deprecated in %s on line %d
 bool(false)
 
-Warning: DOMDocument::loadXML(): I/O warning : failed to load external entity "%s" in %s on line %d
+Warning: DOMDocument::loadXML(): %Sfailed to load external entity "%s" in %s on line %d
 bool(true)
 Done
diff --git a/ext/libxml/tests/libxml_set_external_entity_loader_variation2.phpt b/ext/libxml/tests/libxml_set_external_entity_loader_variation2.phpt
index 9ebf2c0e9d32..5657b727bacd 100644
--- a/ext/libxml/tests/libxml_set_external_entity_loader_variation2.phpt
+++ b/ext/libxml/tests/libxml_set_external_entity_loader_variation2.phpt
@@ -39,6 +39,8 @@ echo "Done.\n";
 string(10) "-//FOO/BAR"
 string(%d) "%sfoobar.dtd"
 
+Warning: DOMDocument::validate(): Failed to load external entity "-//FOO/BAR" in %s on line %d
+
 Warning: DOMDocument::validate(): Could not load the external subset "foobar.dtd" in %s on line %d
 bool(false)
 bool(true)
diff --git a/ext/xml/tests/bug26614_libxml_gte2_11.phpt b/ext/xml/tests/bug26614_libxml_gte2_11.phpt
new file mode 100644
index 000000000000..9a81b67686d1
--- /dev/null
+++ b/ext/xml/tests/bug26614_libxml_gte2_11.phpt
@@ -0,0 +1,95 @@
+--TEST--
+Bug #26614 (CDATA sections skipped on line count)
+--EXTENSIONS--
+xml
+--SKIPIF--
+<?php
+if (!defined("LIBXML_VERSION")) die('skip libxml2 test');
+if (LIBXML_VERSION < 21100) die('skip libxml2 test variant for version >= 2.11');
+?>
+--FILE--
+<?php
+/*
+this test works fine with Expat but fails with libxml
+which we now use as default
+
+further investigation has shown that not only line count
+is skipped on CDATA sections but that libxml does also
+show different column numbers and byte positions depending
+on context and in opposition to what one would expect to
+see and what good old Expat reported just fine ...
+*/
+
+$xmls = array();
+
+// Case 1: CDATA Sections
+$xmls["CDATA"] ='<?xml version="1.0" encoding="iso-8859-1" ?>
+<data>
+<![CDATA[
+multi
+line
+CDATA
+block
+]]>
+</data>';
+
+// Case 2: replace some characters so that we get comments instead
+$xmls["Comment"] ='<?xml version="1.0" encoding="iso-8859-1" ?>
+<data>
+<!-- ATA[
+multi
+line
+CDATA
+block
+-->
+</data>';
+
+// Case 3: replace even more characters so that only textual data is left
+$xmls["Text"] ='<?xml version="1.0" encoding="iso-8859-1" ?>
+<data>
+-!-- ATA[
+multi
+line
+CDATA
+block
+---
+</data>';
+
+function startElement($parser, $name, $attrs) {
+    printf("<$name> at line %d, col %d (byte %d)\n",
+               xml_get_current_line_number($parser),
+               xml_get_current_column_number($parser),
+               xml_get_current_byte_index($parser));
+}
+
+function endElement($parser, $name) {
+    printf("</$name> at line %d, col %d (byte %d)\n",
+               xml_get_current_line_number($parser),
+               xml_get_current_column_number($parser),
+               xml_get_current_byte_index($parser));
+}
+
+function characterData($parser, $data) {
+  // dummy
+}
+
+foreach ($xmls as $desc => $xml) {
+  echo "$desc\n";
+    $xml_parser = xml_parser_create();
+    xml_set_element_handler($xml_parser, "startElement", "endElement");
+    xml_set_character_data_handler($xml_parser, "characterData");
+    if (!xml_parse($xml_parser, $xml, true))
+        echo "Error: ".xml_error_string(xml_get_error_code($xml_parser))."\n";
+    xml_parser_free($xml_parser);
+}
+?>
+--EXPECTF--
+CDATA
+<DATA> at line 2, col %d (byte 50)
+</DATA> at line 9, col %d (byte 96)
+Comment
+<DATA> at line 2, col %d (byte 50)
+</DATA> at line 9, col %d (byte 96)
+Text
+<DATA> at line 2, col %d (byte 50)
+</DATA> at line 9, col %d (byte 96)
diff --git a/ext/xml/tests/bug26614_libxml.phpt b/ext/xml/tests/bug26614_libxml_pre2_11.phpt
similarity index 96%
rename from ext/xml/tests/bug26614_libxml.phpt
rename to ext/xml/tests/bug26614_libxml_pre2_11.phpt
index 6acf2c44b2a6..c581a08e9b8f 100644
--- a/ext/xml/tests/bug26614_libxml.phpt
+++ b/ext/xml/tests/bug26614_libxml_pre2_11.phpt
@@ -5,6 +5,7 @@ xml
 --SKIPIF--
 <?php
 if (!defined("LIBXML_VERSION")) die('skip libxml2 test');
+if (LIBXML_VERSION >= 21100) die('skip libxml2 test variant for version < 2.11');
 ?>
 --FILE--
 <?php
