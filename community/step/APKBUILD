# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=step
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by khtml
# riscv64 disabled due to missing rust in recursive dependency
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/step/"
pkgdesc="Interactive Physics Simulator"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	eigen-dev
	extra-cmake-modules
	kconfig-dev
	kcrash-dev
	kdoctools-dev
	khtml-dev
	kiconthemes-dev
	knewstuff-dev
	kplotting-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	shared-mime-info
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/step-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
d0f373aa90d67cff85ef70974bb37af562edcea53e914fa85b1c11af366c52146d3496f3a7d95fd7159330ce51ddea5c3729d7cae19ae951cd40694a69ee8f37  step-23.04.1.tar.xz
"
