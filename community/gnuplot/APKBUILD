# Contributor: prspkt <prspkt@protonmail.com>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=gnuplot
pkgver=5.4.7
pkgrel=0
pkgdesc="Utility for plotting graphs"
url="http://www.gnuplot.info/"
arch="all"
license="MIT"
makedepends="
	cairo-dev
	gd-dev
	libjpeg-turbo-dev
	libpng-dev
	lua5.3-dev
	pango-dev
	readline-dev
	"
subpackages="$pkgname-doc"
source="https://downloads.sourceforge.net/sourceforge/gnuplot/gnuplot-$pkgver.tar.gz"

build() {
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--mandir=/usr/share/man \
		--localstatedir=/var \
		--enable-stats \
		--disable-wxwidgets \
		--disable-qt
	make
}

check() {
	make GNUTERM=dumb check
}

package() {
	make DESTDIR="$pkgdir" install

	mkdir -p "$pkgdir"/usr/share/doc/$pkgname/
	install -m644 BUGS NEWS PGPKEYS README* \
		"$pkgdir"/usr/share/doc/$pkgname/
}

sha512sums="
78f5af8b76d53202002f9a6e69df23a6384b7b06730b5d3c47d2d580b34b7c7ff317ebb7f052f926eed7a530e27abff62a3ec1838325393c3cc1ddf5200ef12d  gnuplot-5.4.7.tar.gz
"
