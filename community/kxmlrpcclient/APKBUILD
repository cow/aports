# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kxmlrpcclient
pkgver=5.106.0
pkgrel=0
pkgdesc="XML-RPC client library for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://projects.kde.org/projects/kde/pim/kxmlrpcclient"
license="BSD-2-Clause"
depends_dev="
	ki18n-dev
	kio-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/portingAids/kxmlrpcclient-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1c3c0953dbf783042c8f3fdeb577aa4014b842b24177244cc320462f79785fcb8fa6bb9b365a98e1f6e89d01efd872095e93c56d0c8a6bae1205099388db0aa0  kxmlrpcclient-5.106.0.tar.xz
"
