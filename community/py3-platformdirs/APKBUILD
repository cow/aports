# Contributor: Duncan Bellamy <dunk@denkimushi.com>
# Maintainer: Duncan Bellamy <dunk@denkimushi.com>
pkgname=py3-platformdirs
pkgver=3.5.1
pkgrel=0
pkgdesc="Module for determining appropriate platform-specific dirs"
url="https://github.com/platformdirs/platformdirs"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-gpep517 py3-hatchling py3-hatch-vcs"
checkdepends="py3-appdirs py3-pytest py3-pytest-mock"
subpackages="$pkgname-pyc"
source="https://files.pythonhosted.org/packages/source/p/platformdirs/platformdirs-$pkgver.tar.gz"
builddir="$srcdir/platformdirs-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" dist/*.whl
}

sha512sums="
e3aa38bc943f9dc709dfaa3c6fe9e11f3b33d4263d1d41f7349d9926db2e41a36dd43c74ea2175ad89b107601f2dffded0f45e7703b9cb8ca9739945dee3c6fb  platformdirs-3.5.1.tar.gz
"
