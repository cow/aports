# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=tokodon
pkgver=23.04.1
pkgrel=1
pkgdesc="A Mastodon client for Plasma and Plasma Mobile"
url="https://invent.kde.org/network/tokodon/"
# armhf blocked by extra-cmake-modules
# s390x and riscv64 blocked by qqc2-desktop-style
arch="all !armhf !s390x !riscv64"
license="GPL-3.0-only AND CC0-1.0"
depends="
	kirigami-addons
	kirigami2
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	kdbusaddons-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami2-dev
	knotifications-dev
	qqc2-desktop-style-dev
	qt5-qtbase-dev
	qtkeychain-dev
	qt5-qtmultimedia-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qtwebsockets-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/tokodon-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
693783f9a27b9370fe892310680797a46c1a894a718122ae64cb479f15ed3293981adfd83255608e52ec03a25befa76b0cfbd36722d6afb97662880cdfbe3bfd  tokodon-23.04.1.tar.xz
"
