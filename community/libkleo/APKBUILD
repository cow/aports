# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkleo
pkgver=23.04.1
pkgrel=0
pkgdesc="KDE PIM cryptographic library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org"
license="GPL-2.0-or-later"
# TODO: Maybe replace gnupg with specific gnupg subpackages.
depends="gnupg"
makedepends="
	boost-dev
	extra-cmake-modules
	gpgme-dev
	kcodecs-dev
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kitemmodels-dev
	kpimtextedit-dev
	kwidgetsaddons-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkleo-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "newkeyapprovaldialogtest"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d23dfd9ac4b0597f4470d9eef4dd636465f4a8e39c82b5922542eede7539bb1c0ce46435127910ccc8c4a9c2985f2ea25474fa53b79fbc510d550a950b0fa44f  libkleo-23.04.1.tar.xz
"
