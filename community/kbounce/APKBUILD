# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kbounce
pkgver=23.04.1
pkgrel=0
pkgdesc="A single player arcade game with the elements of puzzle"
url="https://kde.org/applications/games/kbounce/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbounce-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
72fd2f5275dd567fdb913d98359b17049b178c75faf553ab7d99b5cb708c5944f9ff015e88b9860a553d82ce3cdb704ab7ba500e5a6b6c44309bdf9fd00830ca  kbounce-23.04.1.tar.xz
"
