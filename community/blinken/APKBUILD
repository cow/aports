# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=blinken
pkgver=23.04.1
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x blocked by kxmlgui
arch="all !armhf !s390x !riscv64"
url="https://edu.kde.org/blinken/"
pkgdesc="Memory Enhancement Game"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcrash-dev
	kdbusaddons-dev
	kdoctools-dev
	kguiaddons-dev
	ki18n-dev
	kxmlgui-dev
	phonon-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/blinken-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
1198ab1d400ce91a2c5081a4ef7eca6c3fda4fc6b87d578db1de31b9a4c147b57ccc211e69e5c4097941350bccceda139ab7d4d0db5670b6c0fe2bf3855e11f9  blinken-23.04.1.tar.xz
"
