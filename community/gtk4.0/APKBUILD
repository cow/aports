# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Pablo Correa Gomez <ablocorrea@hotmail.com>
pkgname=gtk4.0
pkgver=4.10.3
pkgrel=3
pkgdesc="The GTK Toolkit (v4)"
url="https://www.gtk.org/"
install="$pkgname.post-install $pkgname.post-upgrade $pkgname.post-deinstall"
arch="all"
options="!check" # Test suite is known to fail upstream
license="LGPL-2.1-or-later"
subpackages="$pkgname-dbg $pkgname-demo $pkgname-dev $pkgname-doc $pkgname-lang"
depends="shared-mime-info gtk-update-icon-cache tzdata iso-codes"
depends_dev="
	at-spi2-core-dev
	gdk-pixbuf-dev
	glib-dev
	libepoxy-dev
	libxext-dev
	libxi-dev
	libxinerama-dev
	libxkbcommon-dev
	vulkan-headers
	wayland-dev
	wayland-protocols
	"
makedepends="
	$depends_dev
	cairo-dev
	colord-dev
	cups-dev
	expat-dev
	fontconfig-dev
	gettext-dev
	gi-docgen
	gnutls-dev
	gobject-introspection-dev
	graphene-dev
	gst-plugins-bad-dev
	gstreamer-dev
	gtk-doc>=1.33
	iso-codes-dev
	libice-dev
	libx11-dev
	libxcomposite-dev
	libxcursor-dev
	libxdamage-dev
	libxfixes-dev
	libxrandr-dev
	meson
	pango-dev
	perl
	py3-docutils
	sassc
	tiff-dev
	vulkan-loader-dev
	zlib-dev
	"
source="https://download.gnome.org/sources/gtk/${pkgver%.*}/gtk-$pkgver.tar.xz
	gtktreeviewmodel-1.patch
	gtktreeviewmodel-2.patch
	nautilus-width.patch
	x11-rendering.patch
	"
builddir="$srcdir/gtk-$pkgver"

build() {
	CFLAGS="$CFLAGS -O2" \
	CPPFLAGS="$CPPFLAGS -O2" \
	abuild-meson \
		-Dgtk_doc=true \
		-Dintrospection=enabled \
		-Dbroadway-backend=true \
		-Dman-pages=true \
		-Dbuild-tests=false \
		-Dbuild-testsuite=false \
		. output
	meson compile -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output

	# use gtk-update-icon-cache from gtk+2.0 for now. The icon cache is forward
	# compatible so this is fine.
	# do the same for gtk4-update-icon-cache
	rm -f "$pkgdir"/usr/bin/gtk-update-icon-cache
	rm -f "$pkgdir"/usr/bin/gtk4-update-icon-cache
	rm -f "$pkgdir"/usr/share/man/man1/gtk-update-icon-cache.1
}

demo() {
	pkgdesc="$pkgdesc (demonstration application)"
	amove \
		usr/bin/gtk4-demo \
		usr/bin/gtk4-demo-application \
		usr/bin/gtk4-icon-browser \
		usr/bin/gtk4-node-editor \
		usr/bin/gtk4-print-editor \
		usr/bin/gtk4-widget-factory \
		usr/share/applications/org.gtk.Demo4.desktop \
		usr/share/applications/org.gtk.IconBrowser4.desktop \
		usr/share/applications/org.gtk.PrintEditor4.desktop \
		usr/share/applications/org.gtk.WidgetFactory4.desktop \
		usr/share/applications/org.gtk.gtk4.NodeEditor.desktop \
		usr/share/glib-2.0/schemas/org.gtk.Demo4.gschema.xml \
		usr/share/gtk-4.0/gtk4builder.rng \
		usr/share/icons \
		usr/share/metainfo
}

dev() {
	default_dev

	amove \
		usr/bin/gtk4-builder-tool \
		usr/bin/gtk4-encode-symbolic-svg \
		usr/bin/gtk4-query-settings \
		usr/share/gtk-4.0/valgrind
}

lang() {
	default_lang

	# These are some localized emoji labels.
	amove usr/share/gtk-4.0/emoji
}

sha512sums="
aac4703f9c135271febdd373b24995dabcd08fb0cea2d6014c812f2839ae88926c1939ddcc64ab9728ba3248a1f58260ce73fa6ef5679e54dbb798021adfa324  gtk-4.10.3.tar.xz
f8798c584b77df87132d76cbc7bb4d2109aa2b2a5077496705c193f4de2a0d8794b986ffed91fcb35707b5e66a70b6fe59a4e98b1323f4fad2ed37630bd8c82f  gtktreeviewmodel-1.patch
afdea0f061668a28a96f0d60aae4bfcb15a4399a1fb96ffd86bc47bca7c931fcfe6c577ed106ff3f680138e73a3127d1d4ef0d52ab4e0f0e4b7e89c4fb867c90  gtktreeviewmodel-2.patch
07d18200533a153ab9db09d3dc81d929645388e5092087976ef9e69e89b4cfb9665c07a9495cc5c042db9ea600039d72a3c10c4568afe0dadba55403ba5d7b23  nautilus-width.patch
18e9cc564ec424787f7bd787261fd6be6ec5012f242c6aeb77b0c99353b3fb130954732e24618410d1bc7d528ad81f4e81e35abe9bd8f42ebfd920715b7e0268  x11-rendering.patch
"
