# Contributor: Fabian Affolter <fabian@affolter-engineering.ch>
# Maintainer: Fabian Affolter <fabian@affolter-engineering.ch>
pkgname=py3-zeroconf
pkgver=0.63.0
pkgrel=0
pkgdesc="Python implementation of multicast DNS service discovery"
url="https://github.com/jstasiak/python-zeroconf"
arch="all"
license="LGPL-2.0-or-later"
replaces="py-zeroconf" # for backwards compatibility
provides="py-zeroconf=$pkgver-r$pkgrel" # for backwards compatibility
depends="python3 py3-ifaddr"
makedepends="
	cython
	py3-gpep517
	py3-poetry-core
	py3-setuptools
	py3-wheel
	python3-dev
	"
checkdepends="py3-pytest py3-pytest-cov py3-pytest-asyncio"
subpackages="$pkgname-pyc"
source="python-zeroconf-$pkgver.tar.gz::https://github.com/jstasiak/python-zeroconf/archive/refs/tags/$pkgver.tar.gz"
builddir="$srcdir"/python-zeroconf-$pkgver

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
346f991409bb8ee6d4a6f981df5b13d4ed8f946977bfa1a6f499d16a689869c9c87baba6b97d64168282b930d7ba41727f6d13e4fe9f7df9767ff78839f2a520  python-zeroconf-0.63.0.tar.gz
"
