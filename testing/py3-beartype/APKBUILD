# Contributor: Galen Abell <galen@galenabell.com>
# Maintainer: Galen Abell <galen@galenabell.com>
pkgname=py3-beartype
_pyname=beartype
pkgver=0.14.0
pkgrel=0
pkgdesc="Fast, pure-Python type checker"
url="https://github.com/beartype/beartype"
arch="noarch"
license="MIT"
depends="python3"
makedepends="py3-setuptools"
checkdepends="py3-pytest py3-numpy py3-mypy py3-docutils py3-sphinx"
subpackages="$pkgname-pyc"
source="
	$_pyname-$pkgver.tar.gz::https://github.com/beartype/beartype/archive/refs/tags/v$pkgver.tar.gz
	"
builddir="$srcdir/$_pyname-$pkgver"

build() {
	python3 setup.py build
}

check() {
	# nuitka isn't packaged for alpine yet
	# pyright isn't available on all architectures
	# long type-check test fails on some architectures
	pytest \
		--deselect beartype_test/a90_func/z90_lib/a90_nuitka/test_nuitka.py \
		--deselect beartype_test/a90_func/pep/test_pep561_static.py::test_pep561_pyright \
		--deselect beartype_test/a00_unit/a90_decor/test_decorwrapper.py::test_wrapper_fail_obj_large
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}
sha512sums="
120b8e9ab54ba35db125696ef0167f5ea15b20e7b401c38716590aad47843020055ed4d602f7c0e562cbb189ba9f5f0e63a15ec20337af190c9ddefb7bd46de8  beartype-0.14.0.tar.gz
"
