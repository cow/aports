# Contributor: Thomas Kienlen <kommander@laposte.net>
# Maintainer: Thomas Kienlen <kommander@laposte.net>
pkgname=spotify-player
pkgver=0.14.0
pkgrel=0
pkgdesc="Command driven spotify player"
url="https://github.com/aome510/spotify-player"
arch="x86_64 aarch64 x86 ppc64le" # limited by rust/cargo, does not compile on armv7, armhf
license="MIT"
makedepends="
	alsa-lib-dev
	cargo
	cargo-auditable
	dbus-dev
	openssl-dev
	wayland-dev
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/aome510/spotify-player/archive/v$pkgver.tar.gz"


prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	cargo auditable build --release --frozen \
		--no-default-features --features 'alsa-backend,media-control,notify,streaming,daemon'
}

check() {
	cargo test --frozen
}

package() {
	install -Dm755 target/release/spotify_player -t "$pkgdir"/usr/bin
}

sha512sums="
38a76271f76c6b4bae1507e313b689c914c3883f3dea89f242bc1d3fead5233dcc35f08501a28f6880cbc1f97ed44f067ef01ae53a14065e3f1d8f03d6f56e8c  spotify-player-0.14.0.tar.gz
"
