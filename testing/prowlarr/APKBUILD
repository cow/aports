# Contributor: Fabricio Silva <hi@fabricio.dev>
# Maintainer: Fabricio Silva <hi@fabricio.dev>
pkgname=prowlarr
pkgver=1.4.1.3258
pkgrel=0
pkgdesc="Aggregator for usenet index and torrent tracker."
url="https://github.com/Prowlarr/Prowlarr"
arch="x86_64 aarch64 armv7"
license="GPL-3.0-only"
options="net !check" # no tests
depends="
	aspnetcore6-runtime
	sqlite-libs
	"
makedepends="
	dotnet6-sdk
	yarn
	"
subpackages="$pkgname-openrc"
install="$pkgname.pre-install"
source="
	$pkgname-$pkgver.tar.gz::https://github.com/Prowlarr/Prowlarr/archive/refs/tags/v$pkgver.tar.gz
	0001-disable-restart.patch
	prowlarr.initd
	prowlarr.confd
	package_info
	"
builddir="$srcdir/Prowlarr-$pkgver"
pkgusers="prowlarr"
pkggroups="prowlarr"

# map arch to dotnet
case $CARCH in
	x86_64) _dotnet_arch="x64" ;;
	aarch64) _dotnet_arch="arm64" ;;
	armv7) _dotnet_arch="arm" ;;
	*) _dotnet_arch="$CARCH" ;;
esac

# custom variables
_runtime="linux-musl-$_dotnet_arch"
_framework="net6.0"
_output="_output"
_artifacts="$_output/$_framework/$_runtime/publish"

prepare() {
	default_prepare

	# increase max opened files
	ulimit -n 4096

	# replace version info
	local buildprops=src/Directory.Build.props
	sed -i -e "s/<AssemblyVersion>[0-9.*]\+<\/AssemblyVersion>/<AssemblyVersion>$pkgver<\/AssemblyVersion>/g" "$buildprops"
	sed -i -e "s/<AssemblyConfiguration>[\$()A-Za-z-]\+<\/AssemblyConfiguration>/<AssemblyConfiguration>master<\/AssemblyConfiguration>/g" "$buildprops"
}

build() {
	# build the package
	dotnet build src \
		-p:RuntimeIdentifiers="$_runtime" \
		-p:Configuration=Release \
		-p:SelfContained=false \
		-t:PublishAllRids

	# remove service helpers
	rm -f "$_artifacts"/ServiceUninstall.*
	rm -f "$_artifacts"/ServiceInstall.*
	# remove Prowlarr.Windows
	rm -f "$_artifacts"/Prowlarr.Windows.*

	# build web ui
	export BROWSERSLIST_IGNORE_OLD_DATA=true
	yarn install --frozen-lockfile --network-timeout 120000
	yarn build --env production --no-stats

	# move web ui to artifacts folder
	mv "$_output"/UI "$_artifacts"
}

package() {
	local DESTDIR="$pkgdir"/usr/lib/prowlarr

	# use package_info to disable update feature
	install -Dm644 "$srcdir"/package_info "$DESTDIR"/package_info
	echo "PackageVersion=$pkgver-r$pkgrel" >>"$DESTDIR"/package_info

	cp -af "$_artifacts" "$DESTDIR"/bin
	chown -R "$pkgusers:$pkggroups" "$DESTDIR"

	install -Dm755 "$srcdir"/$pkgname.initd "$pkgdir"/etc/init.d/$pkgname
	install -Dm644 "$srcdir"/$pkgname.confd "$pkgdir"/etc/conf.d/$pkgname
}

sha512sums="
d192257d33401bbc3a7b47486a21b6c3d17b850d0668dca44aba4d535fbe671617d425a1d59e1ca56c7ed3a028912afc1cc03f5eb4a2496a9d277df2beb0e7ad  prowlarr-1.4.1.3258.tar.gz
753520379a6c5f2899b4ddfd820d5573598a32cb105f74c6fd978a3f2c98895cf709ef2d9afe2bae8c4856da86cb6a1364f5713ea7ef6a36bb21b5a5840c1471  0001-disable-restart.patch
944467713c1fc747b21e731cbadc3c7e6d0e4d65992a989e154f9ff954555e46553aa03398f1f7990bdf972afc2af276d433d504b4745ec96adb7d7c66fb7a7b  prowlarr.initd
82085e127eef170c2396076970c01b71c7e627ff3bbbf69fab4b83c59d2bb9a591cb484f8370fb9db5ccc7779545f949de00c25b9fc6aac4dace00f19cb5a2cf  prowlarr.confd
67c4dd1cf8259296d994c350dfd3962d23333071889ce0b4ef6b2f4cbb4349490c5dbe4dcc202c99bab7a5c4b7611be56d7c8835b2b13924526c45311db1c3fb  package_info
"
